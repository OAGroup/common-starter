package com.yishuifengxiao.common.oauth2.event;

import org.springframework.context.ApplicationEvent;

import com.yishuifengxiao.common.oauth2.entity.TokenInfo;

/**
 * 访问token端点时的消息
 * 
 * @author yishui
 * @date 2019年10月11日
 * @version 1.0.0
 */
public class TokenEndpointEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8848321429246954640L;
	private TokenInfo tokenInfo;

	public TokenInfo getTokenInfo() {
		return tokenInfo;
	}

	public void setTokenInfo(TokenInfo tokenInfo) {
		this.tokenInfo = tokenInfo;
	}

	public TokenEndpointEvent(Object source, TokenInfo tokenInfo) {
		super(source);
		this.tokenInfo = tokenInfo;
	}

}
