package com.yishuifengxiao.common.resourceserver.endpoint;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.yishuifengxiao.common.resourceserver.ResourceProperties;
import com.yishuifengxiao.common.resourceserver.provider.ResourceAuthorizeProvider;
import com.yishuifengxiao.common.security.constant.SessionConstant;
import com.yishuifengxiao.common.security.context.SecurityHolder;
import com.yishuifengxiao.common.security.eunm.HandleEnum;
import com.yishuifengxiao.common.security.processor.ProcessHandler;
import com.yishuifengxiao.common.support.ErrorMsgUtil;
import com.yishuifengxiao.common.tool.entity.Response;

import lombok.extern.slf4j.Slf4j;

/**
 * 异常处理<br/>
 * <br/>
 * 
 * 在<code>ResourceAuthorizeProvider</code>中被配置为资源异常处理方式
 * 
 * @see ResourceAuthorizeProvider
 * @author yishui
 * @version 1.0.0
 * @date 2019-10-29
 */
@Slf4j
public class ResourceAuthenticationEntryPoint implements AuthenticationEntryPoint {

	/**
	 * 声明了缓存与恢复操作
	 */
	private RequestCache cache = new HttpSessionRequestCache();

	private ResourceProperties resourceProperties;

	private ErrorMsgUtil errorMsgUtil;
	/**
	 * 协助处理器
	 */
	private ProcessHandler customHandle;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		// 引起跳转的uri
		SavedRequest savedRequest = cache.getRequest(request, response);
		String url = savedRequest != null ? savedRequest.getRedirectUrl() : request.getRequestURL().toString();
		// 存储消息到session中
		request.getSession().setAttribute(SessionConstant.EXCEPTION_MSG, authException);
		// 将被拦截的url存放到session中
		request.getSession().setAttribute(SessionConstant.EXCEPTION_URL, url);
		// 存储异常信息
		SecurityHolder.getContext().setSecurityExcepion(request, authException);
		log.debug("【资源服务】获取资源{}失败, 失败的原因为 {}", url, authException);

		// 异常提示信息
		String msg = this.errorMsgUtil.getErrorMsg(authException);

		customHandle.handle(request, response, resourceProperties.getReturnType() == HandleEnum.REDIRECT,
				resourceProperties.getRedirectUrl(),
				new Response<>(Response.Const.CODE_UNAUTHORIZED, msg, authException));
	}

	public ResourceProperties getResourceProperties() {
		return resourceProperties;
	}

	public void setResourceProperties(ResourceProperties resourceProperties) {
		this.resourceProperties = resourceProperties;
	}

	public ProcessHandler getCustomHandle() {
		return customHandle;
	}

	public void setCustomHandle(ProcessHandler customHandle) {
		this.customHandle = customHandle;
	}

	public ErrorMsgUtil getErrorMsgUtil() {
		return errorMsgUtil;
	}

	public void setErrorMsgUtil(ErrorMsgUtil errorMsgUtil) {
		this.errorMsgUtil = errorMsgUtil;
	}

}
