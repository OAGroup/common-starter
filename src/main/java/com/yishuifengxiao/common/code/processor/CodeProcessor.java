/**
 * 
 */
package com.yishuifengxiao.common.code.processor;

import org.springframework.web.context.request.ServletWebRequest;

import com.yishuifengxiao.common.code.entity.ValidateCode;
import com.yishuifengxiao.common.code.eunm.CodeType;
import com.yishuifengxiao.common.tool.exception.ValidateException;

/**
 * 验证码处理器
 * 
 * @author yishui
 * @date 2019年1月22日
 * @version v1.0.0
 */
public interface CodeProcessor {
	/**
	 * 创建校验码并发送验证码
	 * 
	 * @param request  ServletWebRequest
	 * @param codeType 验证码的类型
	 * @return 创建并发送的验证码
	 * @throws ValidateException
	 */
	ValidateCode create(ServletWebRequest request, CodeType codeType) throws ValidateException;

	/**
	 * 校验验证码
	 * 
	 * @param request  ServletWebRequest
	 * @param codeType 验证码的类型
	 * @throws ValidateException
	 */
	void validate(ServletWebRequest request, CodeType codeType) throws ValidateException;
}
