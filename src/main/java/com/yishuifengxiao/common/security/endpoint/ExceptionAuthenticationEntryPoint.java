/**
 * 
 */
package com.yishuifengxiao.common.security.endpoint;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.yishuifengxiao.common.oauth2.Oauth2Resource;
import com.yishuifengxiao.common.oauth2.Oauth2Server;
import com.yishuifengxiao.common.security.SecurityProperties;
import com.yishuifengxiao.common.security.authorize.provider.impl.ExceptionAuthorizeProvider;
import com.yishuifengxiao.common.security.authorize.provider.impl.HttpBasicAuthorizeProvider;
import com.yishuifengxiao.common.security.constant.SessionConstant;
import com.yishuifengxiao.common.security.context.SecurityHolder;
import com.yishuifengxiao.common.security.eunm.HandleEnum;
import com.yishuifengxiao.common.security.event.ExceptionAuthenticationEntryPointEvent;
import com.yishuifengxiao.common.security.processor.ProcessHandler;
import com.yishuifengxiao.common.security.utils.HttpUtil;
import com.yishuifengxiao.common.support.ErrorMsgUtil;
import com.yishuifengxiao.common.tool.entity.Response;

import lombok.extern.slf4j.Slf4j;

/**
 * 当参数中不存在token时的提示信息 处理器<br/>
 * 
 * <strong>主要解决不存在access_token导致401的问题</strong><br/>
 * 
 * 参见 https://www.cnblogs.com/mxmbk/p/9782409.html<br/>
 * 
 * 该类主要解决以下问题：<br/>
 * 
 * <pre>
 * 
  * 问题：测试发现授权接口，当请求参数中不存在access_token时发现接口返回错误信息：
 *               {"timestamp":1539337154336,"status":401,"error":"Unauthorized","message":"No message available","path":"/app/businessCode/list"}
 * 
   *   排查：经过前面的分析发现，上面提到Security的FilterSecurityInterceptor对OAuth2中返回的信息和本身配置校验后，抛出AccessDenyException。
 * 
   *  解决：经过上面的几个问题的处理，发现思路还是一样的，需要定义响应结果，
 * 
  *                 即1、自定义响应处理逻辑SecurityAuthenticationEntryPoint 2、自定义处理逻辑SecurityAuthenticationEntryPoint生效（见上面的配置）
 * 
 * </pre>
 * 
 * 该配置会被被两处配置收集：<br/>
 * 1 在<code>ExceptionAuthorizeProvider</code>中被配置为异常处理方式<br/>
 * 2 在<code>HttpBasicAuthorizeProvider</code>中被配置为异常处理方式<br/>
 * 3
 * 被<code>Oauth2Resource</code>收集，然后经<code>public void configure(ResourceServerSecurityConfigurer resources) </code>注入到oauth2中<br/>
 * 4
 * 被<code>Oauth2Server</code>收集，然后经<code>public void configure(AuthorizationServerSecurityConfigurer security)</code>注入到oauth2中
 * 
 * @see HttpBasicAuthorizeProvider
 * @see ExceptionAuthorizeProvider
 * @see Oauth2Resource
 * @see Oauth2Server
 * @author yishui
 * @Date 2019年4月2日
 * @version 1.0.0
 */
@Slf4j
public class ExceptionAuthenticationEntryPoint extends Http403ForbiddenEntryPoint {

	/**
	 * 声明了缓存与恢复操作
	 */
	private RequestCache cache = new HttpSessionRequestCache();

	private SecurityProperties securityProperties;

	private ErrorMsgUtil errorMsgUtil;

	/**
	 * 协助处理器
	 */
	private ProcessHandler customHandle;

	private ApplicationContext context;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		// 发布信息
		context.publishEvent(new ExceptionAuthenticationEntryPointEvent(this, request, authException));
		// 引起跳转的uri
		SavedRequest savedRequest = cache.getRequest(request, response);
		String url = savedRequest != null ? savedRequest.getRedirectUrl() : request.getRequestURL().toString();
		// 存储消息到session中
		request.getSession().setAttribute(SessionConstant.EXCEPTION_MSG, authException);
		// 将被拦截的url存放到session中
		request.getSession().setAttribute(SessionConstant.EXCEPTION_URL, url);
		// 存储异常信息
		SecurityHolder.getContext().setSecurityExcepion(request, authException);

		// 获取系统的处理方式
		HandleEnum handleEnum = securityProperties.getHandler().getException().getReturnType();

		HandleEnum type = HttpUtil.handleType(request, securityProperties.getHandler(), handleEnum);

		log.debug("【资源服务】获取资源 失败(可能是缺少token),该资源的url为 {}", request.getRequestURL().toString());
		log.debug("【资源服务】获取资源 失败(可能是缺少token), 系统配置的处理方式为 {} ,实际的处理方式为 {} , 失败的原因为 {} ", handleEnum, type,
				authException);

		if (type == HandleEnum.DEFAULT) {
			super.commence(request, response, authException);
			return;
		}

		// 异常提示信息
		String msg = this.errorMsgUtil.getErrorMsg(authException, "请求需要认证");

		customHandle.handle(request, response, type == HandleEnum.REDIRECT,
				securityProperties.getHandler().getException().getRedirectUrl(),
				new Response<>(Response.Const.CODE_UNAUTHORIZED, msg, authException));

	}

	public ExceptionAuthenticationEntryPoint(SecurityProperties securityProperties, ErrorMsgUtil errorMsgUtil,
			ProcessHandler customHandle, ApplicationContext context) {
		this.securityProperties = securityProperties;
		this.errorMsgUtil = errorMsgUtil;
		this.customHandle = customHandle;
		this.context = context;
	}

	public ExceptionAuthenticationEntryPoint() {

	}

	public SecurityProperties getSecurityProperties() {
		return securityProperties;
	}

	public void setSecurityProperties(SecurityProperties securityProperties) {
		this.securityProperties = securityProperties;
	}

	public ProcessHandler getCustomHandle() {
		return customHandle;
	}

	public void setCustomHandle(ProcessHandler customHandle) {
		this.customHandle = customHandle;
	}

	public ApplicationContext getContext() {
		return context;
	}

	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	public ErrorMsgUtil getErrorMsgUtil() {
		return errorMsgUtil;
	}

	public void setErrorMsgUtil(ErrorMsgUtil errorMsgUtil) {
		this.errorMsgUtil = errorMsgUtil;
	}

}
