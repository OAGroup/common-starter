package com.yishuifengxiao.common.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.yishuifengxiao.common.security.SecurityProperties;
import com.yishuifengxiao.common.security.constant.SessionConstant;
import com.yishuifengxiao.common.security.context.SecurityHolder;
import com.yishuifengxiao.common.security.eunm.HandleEnum;
import com.yishuifengxiao.common.security.event.AuthenticationFailureEvent;
import com.yishuifengxiao.common.security.processor.ProcessHandler;
import com.yishuifengxiao.common.security.utils.HttpUtil;
import com.yishuifengxiao.common.support.ErrorMsgUtil;
import com.yishuifengxiao.common.tool.entity.Response;

import lombok.extern.slf4j.Slf4j;

/**
 * 登陆失败处理器
 * <hr/>
 * 1 采用实现AuthenticationFailureHandler接口的方法 <br/>
 * 2 采用继承 SimpleUrlAuthenticationFailureHandler 的方法
 * 
 * @author admin
 *
 */
@Slf4j
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	/**
	 * 自定义属性配置
	 */
	private SecurityProperties securityProperties;

	private ErrorMsgUtil errorMsgUtil;
	/**
	 * 协助处理器
	 */
	private ProcessHandler customProcessor;

	private ApplicationContext context;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		log.debug("【认证服务】登录失败，失败的原因为 {}", exception.getMessage());
		// 发布事件
		context.publishEvent(new AuthenticationFailureEvent(this, request, exception));

		// 存储消息到session中
		request.getSession().setAttribute(SessionConstant.FAIL_MSG, exception);
		// 存储异常信息
		SecurityHolder.getContext().setSecurityExcepion(request, exception);

		// 获取系统的处理方式
		HandleEnum handleEnum = securityProperties.getHandler().getFail().getReturnType();

		HandleEnum type = HttpUtil.handleType(request, securityProperties.getHandler(), handleEnum);

		log.debug("【认证服务】登录失败，系统配置的处理方式为 {},最终的处理方式为 {}，失败的原因为 {}", handleEnum, type, exception);
		// 判断是否使用系统的默认处理方法
		if (type == HandleEnum.DEFAULT) {
			super.onAuthenticationFailure(request, response, exception);
			return;
		}

		// 异常提示信息
		String msg = this.errorMsgUtil.getErrorMsg(exception, "用户名或密码不正确");

		customProcessor.handle(request, response, type == HandleEnum.REDIRECT,
				securityProperties.getHandler().getFail().getRedirectUrl(),
				new Response<>(Response.Const.CODE_INTERNAL_SERVER_ERROR, msg, exception));

	}

	public CustomAuthenticationFailureHandler() {

	}

	public CustomAuthenticationFailureHandler(SecurityProperties securityProperties, ErrorMsgUtil errorMsgUtil,
			ProcessHandler customProcessor, ApplicationContext context) {
		this.securityProperties = securityProperties;
		this.errorMsgUtil = errorMsgUtil;
		this.customProcessor = customProcessor;
		this.context = context;
	}

	public SecurityProperties getSecurityProperties() {
		return securityProperties;
	}

	public void setSecurityProperties(SecurityProperties securityProperties) {
		this.securityProperties = securityProperties;
	}

	public ProcessHandler getCustomHandle() {
		return customProcessor;
	}

	public void setCustomHandle(ProcessHandler customProcessor) {
		this.customProcessor = customProcessor;
	}

	public ApplicationContext getContext() {
		return context;
	}

	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	public ProcessHandler getCustomProcessor() {
		return customProcessor;
	}

	public void setCustomProcessor(ProcessHandler customProcessor) {
		this.customProcessor = customProcessor;
	}

	public ErrorMsgUtil getErrorMsgUtil() {
		return errorMsgUtil;
	}

	public void setErrorMsgUtil(ErrorMsgUtil errorMsgUtil) {
		this.errorMsgUtil = errorMsgUtil;
	}

}