/**
 * 
 */
package com.yishuifengxiao.common.security.authorize;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * <strong>授权配置管理器</strong><br/>
 * <br/>
 * 收集系统中所有的授权提供器(<code>AuthorizeProvider</code>)并配置到ExpressionInterceptUrlRegistry中。<br/>
 * 
 * <pre>
 * ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry
 * config=httpSecurity.authorizeRequests()
 * 
 * authorizeConfigProvider.config(config)
 * 
 * <pre>
 * 处理完收集的<code>AuthorizeProvider</code>后，AuthorizeConfigManager 会将自己注入到
 * 安全管理器(<code>SecurityContextManager</code>)中，由<code>SecurityContextManager</code>注入到spring
 * security中。 <br/>
 * 
 * @see AuthorizeProvider
 * @see SecurityContextManager
 * @author yishui
 * @date 2019年1月8日
 * @version 0.0.1
 */
public interface AuthorizeConfigManager {
	/**
	 * 收集系统中的所有授权配
	 * 
	 * @param config
	 */
	void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config);
}
