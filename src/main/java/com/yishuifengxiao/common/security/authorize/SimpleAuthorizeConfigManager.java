/**
 * 
 */
package com.yishuifengxiao.common.security.authorize;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

import com.yishuifengxiao.common.security.authorize.provider.AuthorizeProvider;

/**
 * 简单实现的授权配置管理器<br/>
 * <br/>
 * 
 * 将系统中配置的<code>AuthorizeProvider</code>所有实例先由小到大排序，然后注入配置到ExpressionInterceptUrlRegistry中。
 * 
 * @author yishui
 * @date 2019年1月8日
 * @version 0.0.1
 */
public class SimpleAuthorizeConfigManager implements AuthorizeConfigManager {
	private final static Logger log = LoggerFactory.getLogger(SimpleAuthorizeConfigManager.class);
	/**
	 * 收集到所有的授权配置，Order的值越小，实例排在队列的越前面，这里需要使用有序队列
	 */
	private List<AuthorizeProvider> authorizeConfigProviders;

	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		if (authorizeConfigProviders != null) {

			authorizeConfigProviders = authorizeConfigProviders.parallelStream().filter(Objects::nonNull)
					.sorted(Comparator.comparing(AuthorizeProvider::getOrder)).collect(Collectors.toList());

			log.debug("【授权管理器】 系统中所有的授权提供器为 {}", authorizeConfigProviders);

			for (AuthorizeProvider authorizeConfigProvider : authorizeConfigProviders) {
				log.debug("【授权管理器】 系统中当前加载的授权提供器序号为 {} , 实例为 {}", authorizeConfigProvider.getOrder(),
						authorizeConfigProvider);

				try {
					authorizeConfigProvider.config(config);
				} catch (Exception e) {
					log.error("【授权管理器】 装载授权配置{}时出现问题，出现问题的原因为 {}", authorizeConfigProvider.getOrder(), e.getMessage());
				}

			}

		}
	}

	public List<AuthorizeProvider> getAuthorizeConfigProviders() {
		return authorizeConfigProviders;
	}

	public void setAuthorizeConfigProviders(List<AuthorizeProvider> authorizeConfigProviders) {
		this.authorizeConfigProviders = authorizeConfigProviders;
	}

	public SimpleAuthorizeConfigManager(List<AuthorizeProvider> authorizeConfigProviders) {
		this.authorizeConfigProviders = authorizeConfigProviders;
	}

	public SimpleAuthorizeConfigManager() {

	}

}
