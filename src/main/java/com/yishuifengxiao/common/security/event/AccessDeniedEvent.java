package com.yishuifengxiao.common.security.event;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.AccessDeniedException;

/**
 * 权限拒绝的事件
 * 
 * @author yishui
 * @Date 2019年5月30日
 * @version 1.0.0
 */
public class AccessDeniedEvent extends AbstractEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1240683622233923579L;

	private AccessDeniedException accessDeniedException;

	public AccessDeniedEvent(Object source, HttpServletRequest request, AccessDeniedException accessDeniedException) {
		super(source, request);
		this.accessDeniedException = accessDeniedException;
	}

	public AccessDeniedException getAccessDeniedException() {
		return this.accessDeniedException;
	}

	public void setAccessDeniedException(AccessDeniedException accessDeniedException) {
		this.accessDeniedException = accessDeniedException;
	}

}
