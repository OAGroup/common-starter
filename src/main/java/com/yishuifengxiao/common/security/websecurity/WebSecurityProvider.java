package com.yishuifengxiao.common.security.websecurity;

import org.springframework.security.config.annotation.web.builders.WebSecurity;

/**
 * web安全授权器
 * 
 * @author qingteng
 * @date 2020年10月28日
 * @version 1.0.0
 */
public interface WebSecurityProvider {
	/**
	 * 配置WebSecurity 管理
	 * 
	 * @param web
	 * @throws Exception
	 */
	void configure(WebSecurity web) throws Exception;
}
