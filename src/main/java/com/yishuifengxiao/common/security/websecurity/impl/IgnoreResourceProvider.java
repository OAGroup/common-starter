package com.yishuifengxiao.common.security.websecurity.impl;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.WebSecurity;

import com.yishuifengxiao.common.security.SecurityProperties;
import com.yishuifengxiao.common.security.constant.SecurityConstant;
import com.yishuifengxiao.common.security.websecurity.WebSecurityProvider;

/**
 * 忽视资源授权器
 * 
 * @author yishui
 * @date 2019年10月18日
 * @version 1.0.0
 */
public class IgnoreResourceProvider implements WebSecurityProvider {
	/**
	 * 自定义属性配置
	 */
	protected SecurityProperties securityProperties;

	@Override
	public void configure(WebSecurity web) throws Exception {

		// @formatter:off
		web.ignoring()
		.antMatchers(HttpMethod.OPTIONS, "/**")
		.antMatchers(SecurityConstant.Oauth.OAUTH_CHECK_TOKEN)
		//设置忽视目录
		.mvcMatchers(securityProperties.getIgnore().getIgnores())
		.antMatchers(securityProperties.getIgnore().getIgnores())
		;
		// @formatter:on

	}

	public SecurityProperties getSecurityProperties() {
		return securityProperties;
	}

	public void setSecurityProperties(SecurityProperties securityProperties) {
		this.securityProperties = securityProperties;
	}

}
