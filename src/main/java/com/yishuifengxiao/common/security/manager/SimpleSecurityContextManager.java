package com.yishuifengxiao.common.security.manager;

import java.util.List;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import com.yishuifengxiao.common.security.authorize.AuthorizeConfigManager;
import com.yishuifengxiao.common.security.httpsecurity.HttpSecurityInterceptor;

/**
 * 系统安全管理器
 * 
 * @author yishui
 * @date 2019年10月12日
 * @version 1.0.0
 */
public class SimpleSecurityContextManager implements SecurityContextManager {

	private AuthorizeConfigManager authorizeConfigManager;

	private List<HttpSecurityInterceptor> interceptors;

	@Override
	public void config(HttpSecurity http) throws Exception {
		// 将HttpSecurityInterceptor 实例装载到security中
		if (this.interceptors != null) {
			for (HttpSecurityInterceptor securityAdapter : this.interceptors) {
				http.apply(securityAdapter);
			}
		}
		// 加入自定义的授权配置
		authorizeConfigManager.config(http.authorizeRequests());

	}

	public SimpleSecurityContextManager() {

	}
	
	

	public SimpleSecurityContextManager(AuthorizeConfigManager authorizeConfigManager,
			List<HttpSecurityInterceptor> interceptors) {
		this.authorizeConfigManager = authorizeConfigManager;
		this.interceptors = interceptors;
	}

	public AuthorizeConfigManager getAuthorizeConfigManager() {
		return authorizeConfigManager;
	}

	public void setAuthorizeConfigManager(AuthorizeConfigManager authorizeConfigManager) {
		this.authorizeConfigManager = authorizeConfigManager;
	}

	public List<HttpSecurityInterceptor> getInterceptors() {
		return interceptors;
	}

	public void setInterceptors(List<HttpSecurityInterceptor> interceptors) {
		this.interceptors = interceptors;
	}
	
	
	

}
