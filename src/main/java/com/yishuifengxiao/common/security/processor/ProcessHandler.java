/**
 * 
 */
package com.yishuifengxiao.common.security.processor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yishuifengxiao.common.tool.entity.Response;

/**
 * <strong>协助处理器</strong><br/>
 * <br/>
 * 
 * 用于在各种 Handler 中根据情况相应地跳转到指定的页面或者输出json格式的数据<br/>
 * 
 * @see ExceptionAuthenticationEntryPoint
 * @see CustomAccessDeniedHandler
 * @see CustomAuthenticationFailureHandler
 * @see CustomAuthenticationSuccessHandler
 * @see CustomLogoutSuccessHandler
 * 
 * @author yishui
 * @Date 2019年4月2日
 * @version 1.0.0
 */
public interface ProcessHandler {

	/**
	 * 对系统中的各种handler进行协助处理
	 * 
	 * @param request  HttpServletRequest
	 * @param response HttpServletResponse
	 * @param isRedict 是否跳转
	 * @param url      希望跳转的路径
	 * @param result   传输过来的信息,如果是异常时就是异常，如果是正常时就是授权信息
	 * @throws IOException
	 * @throws ServletException
	 */
	@SuppressWarnings("rawtypes")
	public void handle(HttpServletRequest request, HttpServletResponse response, Boolean isRedict, String url,
			Response result) throws IOException;

}
