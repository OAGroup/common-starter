package com.yishuifengxiao.common.security.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.yishuifengxiao.common.security.AbstractSecurityConfig;
import com.yishuifengxiao.common.security.SecurityProperties;
import com.yishuifengxiao.common.security.httpsecurity.HttpSecurityInterceptor;
import com.yishuifengxiao.common.security.httpsecurity.impl.AuthorizeResourceInterceptor;
import com.yishuifengxiao.common.social.SocialProperties;

/**
 * 配置系统中的资源授权拦截器<code>HttpSecurityInterceptor</code><br/>
 * <br/>
 * 功能如下：<br/>
 * 1 配置需要拦截哪些资源<br/>
 * 2 配置异常处理方式<br/>
 * 
 * @author yishui
 * @version 0.0.1
 * @date 2018年6月15日
 */
@Configuration
@ConditionalOnBean(AbstractSecurityConfig.class)
@ConditionalOnClass({ DefaultAuthenticationEventPublisher.class, EnableWebSecurity.class,
		WebSecurityConfigurerAdapter.class , AbstractSecurityConfig.class })
@ConditionalOnProperty(prefix = "yishuifengxiao.security", name = {
		"enable" }, havingValue = "true", matchIfMissing = true)
public class HttpSecurityAutoConfiguration {

	/**
	 * 配置需要拦截哪些资源
	 * 
	 * @param securityProperties
	 * @param socialProperties
	 * @return
	 */
	@Bean("authorizeResourceInterceptor")
	@ConditionalOnMissingBean(name = { "authorizeResourceInterceptor" })
	public HttpSecurityInterceptor authorizeResourceInterceptor(SecurityProperties securityProperties,
			SocialProperties socialProperties) {
		AuthorizeResourceInterceptor authorizeResourceProvider = new AuthorizeResourceInterceptor();
		authorizeResourceProvider.setSecurityProperties(securityProperties);
		authorizeResourceProvider.setSocialProperties(socialProperties);
		return authorizeResourceProvider;
	}


}
