/**
 * 
 */
package com.yishuifengxiao.common.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.yishuifengxiao.common.security.constant.SecurityConstant;
import com.yishuifengxiao.common.security.eunm.HandleEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 安全相关的配置
 * 
 * @author yishui
 * @date 2019年1月5日
 * @version 0.0.1
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "yishuifengxiao.security")
public class SecurityProperties {
	/**
	 * 是否开启安全相关的功能
	 */
	private Boolean enable = true;

	/**
	 * 加解密中需要使用的密钥
	 */
	private String secretKey;
	/**
	 * 关闭csrf功能,默认为true
	 */
	private Boolean closeCsrf = SecurityConstant.Statu.CLOSE_CSRF;
	/**
	 * 是否关闭cors保护，默认为false
	 */
	private Boolean closeCors = SecurityConstant.Statu.CLOSE_CORS;
	/**
	 * 是否开启httpBasic访问，默认为true
	 */
	private Boolean httpBasic = SecurityConstant.Statu.HTTP_BASIC;
	/**
	 * 资源名称,默认为yishuifengxiao
	 */
	private String realmName = SecurityConstant.Oauth.REAL_NAME;

	/**
	 * 处理器的处理方式配置
	 */
	private HandlerProperties handler = new HandlerProperties();
	/**
	 * spring security 核心配置
	 */
	private CoreProperties core = new CoreProperties();
	/**
	 * spring security session相关的配置
	 */
	private SessionProperties session = new SessionProperties();

	/**
	 * 需要自定义权限的路径
	 */
	private CustomAuthProperties custom = new CustomAuthProperties();

	/**
	 * spring security 忽视目录配置
	 */
	private IgnoreProperties ignore = new IgnoreProperties();
	/**
	 * 记住我相关的属性
	 */
	private RemeberMeProperties remeberMe = new RemeberMeProperties();
	/**
	 * 验证码及短信登陆相关配置
	 */
	private ValidateProperties code = new ValidateProperties();
	/**
	 * 所有不经过资源授权管理的的资源路径<br/>
	 * key: 不参与解析，可以为任意值，但必须唯一<br/>
	 * value: 不希望经过授权管理的路径，采用Ant风格匹配,多个路径之间用半角逗号(,)分给开
	 */
	private Map<String, String> excludes = new HashMap<>();

	/**
	 * 获取所有不经过oauth2资源服务器授权管理的路径
	 * 
	 * @return
	 */
	public List<String> getExcludeUrls() {
		List<String> excludeUrls = new ArrayList<>();
		this.excludes.forEach((k, v) -> {
			if (StringUtils.isNotBlank(v)) {
				String[] urls = StringUtils.splitByWholeSeparatorPreserveAllTokens(v, ",");
				for (String url : urls) {
					excludeUrls.add(url);
				}

			}
		});
		return excludeUrls.stream().distinct().collect(Collectors.toList());
	}

	/**
	 * 自定义handler的配置文件
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class HandlerProperties {
		/**
		 * 登陆成功后的处理方式
		 */
		private LoginSucProperties suc = new LoginSucProperties();
		/**
		 * 登陆失败时的处理方式
		 */
		private LoginFailProperties fail = new LoginFailProperties();
		/**
		 * 退出成功后的配置
		 */
		private LoginOutProperties exit = new LoginOutProperties();

		/**
		 * 出现异常时的配置
		 */
		private ExceptionProperties exception = new ExceptionProperties();
		/**
		 * 权限拒绝时的配置
		 */
		private AccessDenieProperties denie = new AccessDenieProperties();
		/**
		 * 从请求头中获取处理方式时的参数名，默认为type
		 */
		private String headerName = SecurityConstant.DEFAULT_HEADER_NAME;
		/**
		 * 从请求参数中获取处理方式时的参数名，默认为 yishuifengxiao
		 */
		private String paramName = SecurityConstant.DEFAULT_PARAM_NAME;

	}

	/**
	 * spring security 核心配置文件类
	 * 
	 * @version 0.0.1
	 * @author yishui
	 * @date 2018年6月29日
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class CoreProperties {
		/**
		 * 表单提交时默认的用户名参数，默认值为 username
		 */
		private String usernameParameter = SecurityConstant.Login.USERNAME_PARAMTER;
		/**
		 * 表单提交时默认的密码名参数，默认值为 pwd
		 */
		private String passwordParameter = SecurityConstant.Login.PASSWORD_PARAMTER;
		/**
		 * 系统登陆页面的地址 ,默认为 /login
		 */
		private String loginPage = SecurityConstant.Url.DEFAULT_LOGIN_URL;
		/**
		 * 权限拦截时默认的跳转地址，默认为 /index
		 */
		private String redirectUrl = SecurityConstant.Url.DEFAULT_REDIRECT_LOGIN_URL;
		/**
		 * 表单登陆时form表单请求的地址，默认为/auth/form
		 */
		private String formActionUrl = SecurityConstant.Url.DEFAULT_FORM_ACTION_URL;

		/**
		 * 默认的处理登出请求的URL的路径【即请求此URL即为退出操作】，默认为/loginOut
		 */
		private String loginOutUrl = SecurityConstant.Url.DEFAULT_LOGINOUT_URL;

	}

	/**
	 * spring security session相关的配置
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SessionProperties {
		/**
		 * 同一个用户在系统中的最大session数，默认1
		 */
		private int maximumSessions = 1;
		/**
		 * 达到最大session时是否阻止新的登录请求，默认为false，不阻止，新的登录会将老的登录失效掉
		 */
		private boolean maxSessionsPreventsLogin = false;
		/**
		 * session失效时跳转的地址
		 */
		private String sessionInvalidUrl = SecurityConstant.Url.DEFAULT_SESSION_INVALID_URL;
	}

	/**
	 * 自定义权限的配置文件
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	public static class CustomAuthProperties {

		/**
		 * 需要自定义授权的路径 <br/>
		 * 键 : 自定义，不参与匹配 <br/>
		 * 值 ：需要匹配的路径，多个路径之间用半角逗号(,)隔开
		 */
		private Map<String, String> urls = new HashMap<>();

		/**
		 * 获取所有需要设置自定义权限的路径
		 * 
		 * @return
		 */
		public Map<String, String> getMap() {
			return urls;
		}

		/**
		 * 设置所有需要设置自定义权限的路径
		 * 
		 * @return
		 */
		public void setMap(Map<String, String> urls) {
			this.urls = urls;
		}

		/**
		 * 获取所有自定义权限的路径
		 * 
		 * @return
		 */
		public List<String> getAll() {
			List<String> list = new ArrayList<>();
			urls.forEach((k, v) -> {
				if (StringUtils.isNotBlank(v)) {
					List<String> customs = Arrays.asList(v.split(",")).parallelStream().filter(StringUtils::isNotBlank)
							.map(t -> t.trim()).collect(Collectors.toList());
					if (customs != null && customs.size() > 0) {
						list.addAll(customs);
					}
				}

			});

			return list;
		}

	}

	/**
	 * spring security忽视目录
	 * 
	 * @author yishui
	 * @date 2019年1月8日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class IgnoreProperties {
		/**
		 * 系统默认包含的静态路径
		 */
		private static String[] staticResource = new String[] { "/js/**", "/css/**", "/images/**", "/fonts/**",
				"/**/**.png", "/**/**.jpg", "/**/**.html", "/**/**.ico", "/**/**.js", "/**/**.css", "/**/**.woff",
				"/**/**.ttf" };

		/**
		 * 系统默认包含的swagger-ui资源路径
		 */
		private static String[] swaagerUiResource = new String[] { "/swagger-ui.html", "/swagger-resources/**",
				"/v2/api-docs" ,"/swagger-ui/**","/v3/**"};
		/**
		 * 系统默认包含actuator相关的路径
		 */
		private static String[] actuatorResource = new String[] { "/actuator/**" };
		/**
		 * 系统默认包含webjars相关的路径
		 */
		private static String[] webjarsResource = new String[] { "/webjars/**" };
		/**
		 * 所有的资源
		 */
		private static String[] allResources = new String[] { "/**" };

		/**
		 * 是否默认包含静态资源
		 */
		private Boolean containStaticResource = true;
		/**
		 * 是否包含swagger-ui的资源
		 */
		private Boolean containSwaagerUiResource = true;
		/**
		 * 是否包含actuator相关的路径
		 */
		private Boolean containActuator = true;
		/**
		 * 是否包含webJars资源
		 */
		private Boolean containWebjars = true;
		/**
		 * 是否包含所有的资源
		 */
		private Boolean containAll = false;

		/**
		 * 所有需要忽视的目录
		 */
		private Map<String, String> urls = new HashMap<>();

		/**
		 * 以字符串形式获取到所有需要忽略的路径
		 * 
		 * @return
		 */
		public String getIgnoreString() {
			return Arrays.asList(this.getIgnores()).parallelStream().filter(StringUtils::isNotBlank)
					.collect(Collectors.joining(", "));

		}

		/**
		 * 获取所有需要忽视的目录
		 * 
		 * @return
		 */
		public String[] getIgnores() {
			Set<String> set = new HashSet<>();
			if (this.containStaticResource) {
				set.addAll(Arrays.asList(staticResource));
			}
			if (this.containSwaagerUiResource) {
				set.addAll(Arrays.asList(swaagerUiResource));
			}
			if (this.containActuator) {
				set.addAll(Arrays.asList(actuatorResource));
			}
			if (this.containWebjars) {
				set.addAll(Arrays.asList(webjarsResource));
			}
			if (this.containAll) {
				set.addAll(Arrays.asList(allResources));
			}
			urls.forEach((k, v) -> {
				if (StringUtils.isNotBlank(v)) {
					List<String> ignores = Arrays.asList(v.split(",")).parallelStream().filter(StringUtils::isNotBlank)
							.map(t -> t.trim()).collect(Collectors.toList());
					set.addAll(ignores);
				}
			});
			return set.toArray(new String[] {});
		}

	}

	/**
	 * 记住我相关的属性配置
	 * 
	 * @author yishui
	 * @date 2019年1月8日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class RemeberMeProperties {
		/**
		 * 是否使用安全cookie
		 */
		private Boolean useSecureCookie = true;
		/**
		 * 记住我产生的token，默认为 yishuifengxiao
		 */
		private String key = SecurityConstant.REMEMBER_ME_AUTHENTICATION_KEY;
		/**
		 * 登陆时开启记住我的参数,默认为 rememberMe
		 */
		private String rememberMeParameter = SecurityConstant.REMEMBER_ME_PARAMTER;

		/**
		 * 默认过期时间为60分钟
		 */
		private Integer rememberMeSeconds = 60 * 60;

	}

	/**
	 * 短信验证码相关属性配置文件
	 * 
	 * @author yishui
	 * @date 2019年1月23日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ValidateProperties {

		/**
		 * 是否过滤GET方法，默认为false
		 */
		private Boolean isFilterGet = false;
		/**
		 * 短信登陆参数,默认为 mobile
		 */
		private String smsLoginParam = SecurityConstant.SMS_LOGIN_PARAM;
		/**
		 * 短信验证码登录地址
		 */
		private String smsLoginUrl;
		/**
		 * 需要过滤的路径<br/>
		 * key：验证码类型的名字<br/>
		 * value: 需要过滤的路径，多个路径采用半角的逗号分隔
		 */
		private Map<String, String> filter = new HashMap<>();

	}

	/**
	 * 权限拒绝时的配置
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class AccessDenieProperties {
		/**
		 * 默认为内容协商
		 */
		private HandleEnum returnType = HandleEnum.AUTO;
		/**
		 * 默认的重定向URL，若该值为空，则跳转到权限拦截的页面
		 */
		private String redirectUrl = SecurityConstant.Url.DEFAULT_REDIRECT_LOGIN_URL;

	}

	/**
	 * 全局异常转换时的数据
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ExceptionProperties {
		/**
		 * 默认为内容协商
		 */
		private HandleEnum returnType = HandleEnum.AUTO;
		/**
		 * 默认的重定向URL，若该值为空，则跳转到权限拦截的页面
		 */
		private String redirectUrl = SecurityConstant.Url.DEFAULT_REDIRECT_LOGIN_URL;

	}

	/**
	 * 登陆失败后的配置
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class LoginFailProperties {
		/**
		 * 默认为内容协商处理
		 */
		private HandleEnum returnType = HandleEnum.AUTO;
		/**
		 * 默认的重定向URL，若该值为空，则跳转到权限拦截的页面 /index
		 */
		private String redirectUrl = SecurityConstant.Url.DEFAULT_REDIRECT_LOGIN_URL;

	}

	/**
	 * 退出成功后的配置
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class LoginOutProperties {
		/**
		 * 默认为重定向
		 */
		private HandleEnum returnType = HandleEnum.AUTO;
		/**
		 * 默认的重定向URL，若该值为空，则跳转到权限拦截的页面 /index
		 */
		private String redirectUrl = SecurityConstant.Url.DEFAULT_REDIRECT_LOGIN_URL;

		/**
		 * 需要删除的cookie的名字 JSESSIONID
		 */
		private String cookieName = SecurityConstant.Login.DEFAULT_COOKIE_NAME;

	}

	/**
	 * 登陆成功后的配置
	 * 
	 * @author yishui
	 * @date 2019年1月5日
	 * @version 0.0.1
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class LoginSucProperties {
		/**
		 * 默认为重定向
		 */
		private HandleEnum returnType = HandleEnum.AUTO;
		/**
		 * 默认的重定向URL，默认为主页地址 /
		 */
		private String redirectUrl = SecurityConstant.Url.DEFAULT_HOME_URL;

	}

}
